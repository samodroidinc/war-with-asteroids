
package warwithasteroids;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

public class BaseSprite extends BaseLayer {

    private BufferedImage[] mFrames;
    private int mFrameIndex;
    private int[] mFrameSequence;
    private java.awt.Rectangle mBounds;
    private AffineTransform trans;
    private int degress, refPixelX, refPixelY;

    public BaseSprite(BufferedImage image) {
        super();
        mFrames = new BufferedImage[]{image};
        mFrameIndex = 0;
        mFrameSequence = new int[]{0};
        mBounds = new java.awt.Rectangle(0, 0, image.getWidth(null), image.getHeight(null));
        trans = new AffineTransform();
        degress = 0;
        refPixelX = this.getWidth() / 2;
        refPixelY = this.getHeight() / 2;
    }

    public BaseSprite(BufferedImage[] images) {
        super();
        mFrames = images;
        mFrameIndex = 0;
        mFrameSequence = new int[images.length];
        for (int n = 0; n < images.length; n++) {
            mFrameSequence[n] = n;
        }
        mBounds = new java.awt.Rectangle(0, 0, images[0].getWidth(null), images[0].getHeight(null));
        trans = new AffineTransform();
        degress = 0;
        refPixelX = this.getWidth() / 2;
        refPixelY = this.getHeight() / 2;
    }

    public boolean collidesWith(BaseSprite s) {
        boolean b=false;
        b=this.getBoundsRect().intersects(s.getBoundsRect());
        return b;
    }

    public boolean collidesWith(Point point) {
        boolean b=false;
        if (this.isVisible()) {
            Rectangle rect = new Rectangle(point);
            b=getBoundsRect().intersects(rect);
        }
        return b;
    }

    public boolean collidesWith(Rectangle rect){
     return this.getBoundsRect().intersects(rect);
    }

    public void defineCollisionRect(Rectangle rect) {
        mBounds.setBounds(rect);
    }

    public Rectangle getBoundsRect() {
        return new java.awt.Rectangle((int)(getX()+mBounds.getX()),(int)(getY()+mBounds.getY()),(int)mBounds.getWidth(),(int)mBounds.getHeight());
    }

    public int getFrameIndex() {
        return mFrameIndex;
    }

    public int getFrameSequenceLength() {
        return mFrameSequence.length;
    }

    public int getHeight() {
        return mFrames[0].getHeight(null);
    }

    public int getRefX() {
        return this.refPixelX;
    }

    public int getRefY() {
        return this.refPixelY;
    }

    public int getTurn() {
        return this.degress;
    }

    public int getWidth() {
        return mFrames[0].getWidth(null);
    }

    public void move(int dx, int dy) {
        super.move(dx, dy);
        trans.setToTranslation(getX(),getY());
        trans.rotate(Math.toRadians(degress),refPixelX,refPixelY);
    }

    public void nextFrame() {
        mFrameIndex++;
        if (mFrameIndex == mFrameSequence.length) {
            mFrameIndex = 0;
        }
    }

    
    public void paint(Graphics2D g2d) {
        if (isVisible()){
            g2d.drawImage(mFrames[mFrameIndex], trans, null);
        }
    }
    
    
    public void prevFrame() {
        mFrameIndex--;
        if (mFrameIndex < 0) {
            mFrameIndex = mFrameSequence.length - 1;
        }
    }

    public void resetRotate() {
        trans.rotate(-Math.toRadians(degress),refPixelX,refPixelY);
        degress = 0;
    }

    public void rotate(int degress) {
        this.degress += degress;
        trans.rotate(Math.toRadians(degress),refPixelX,refPixelY);
        if (this.degress < 0) {
            this.degress = 360 + this.degress;
        }
    }

    public void setFrame(int frame) {
        this.mFrameIndex = frame;
    }

    public void setFrameSequence(int[] frameSequence) {
        mFrameSequence = new int[frameSequence.length];
        System.arraycopy(frameSequence, 0, mFrameSequence, 0,
                frameSequence.length);
    }

    public void setPosition(int x, int y) {
        super.setPosition(x, y);
        trans.setToTranslation(x, y);
        trans.rotate(Math.toRadians(degress),refPixelX,refPixelY);
    }

    public void setRefPixel(int refX, int refY) {
        this.refPixelX = refX;
        this.refPixelY = refY;
    }

}


