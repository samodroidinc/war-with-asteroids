/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package warwithasteroids;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JFrame;


public class Music {
private static Clip audio1; //звук для заставки
private static Clip audio2; //звук игры
private static Clip explose;
    
    public static void playStart1(JFrame jFrame){
        try {
        AudioInputStream audioIS= AudioSystem.getAudioInputStream(jFrame.getClass().getClassLoader().getResource("start.wav"));
        Clip audio1= AudioSystem.getClip();
        audio1.open(audioIS);
        audio1.start();
        } catch (Exception e) {}
            
        }
    
    public static void playStart2(JFrame jFrame){
            try {
            AudioInputStream audioIS= AudioSystem.getAudioInputStream(jFrame.getClass().getClassLoader().getResource("game.wav"));
            Clip audio2= AudioSystem.getClip();
            audio2.open(audioIS);
            audio2.loop(-1);
            audio2.start();
            } catch (Exception e) {}    
        }
   
    
    
     public static void playExplose(JFrame jFrame){
        try {
        AudioInputStream audioIS= AudioSystem.getAudioInputStream(jFrame.getClass().getClassLoader().getResource("explose.wav"));
        Clip audio2= AudioSystem.getClip();
        audio2.open(audioIS);
        audio2.start();
        } catch (Exception e) {}
            
        }
    
    
}
