/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package warwithasteroids;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

/**
 *
 * @author Сергей
 */
public class Ship extends BaseSprite{
    private double degress; //угол поворота вокруг планеты
    private Point center; //центральная точка планеты
    private Rocket rocket; //ракета
    
    public Ship(BufferedImage image,Point center) {
        super(image);
        degress=0;
        this.center=center;
        setPosition((int)((Math.cos(degress)*180)+center.getX()-10),(int)((Math.sin(degress)*180)+center.getY()-10));
        try{
        rocket=new Rocket(ImageIO.read(getClass().getResourceAsStream("/rocket.png")));
        rocket.setPosition(this.getX()+20, this.getY()+20);
        }catch(Exception e){}
    }
    
    public void update(){
        setPosition((int)((Math.cos(degress)*180)+center.getX()-10),(int)((Math.sin(degress)*180)+center.getY()-10));
        if (!rocket.isStarting()){
            rocket.setPosition(this.getX()+20, this.getY()+20);
            rocket.resetRotate();
            rocket.rotate(this.getTurn());
        }
        if (rocket.isStarting()) rocket.update();
        degress+=0.05;
    }
    
    public void startRocket(){
        if (!rocket.isStarting()) rocket.start(this.getTurn());
    }
    
    public void exploseRocket(){
        rocket.explose();
    }
    
    public boolean collidesRocket(BaseSprite sprite){
        return rocket.collidesWith(sprite);
    }
    
    
    public void paint(Graphics2D g2d){
        rocket.paint(g2d);
        super.paint(g2d);
    }
    
}
