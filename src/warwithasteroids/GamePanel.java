/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package warwithasteroids;

import java.awt.Color;
import java.awt.DisplayMode;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 *
 * @author Сергей
 */
public class GamePanel extends JPanel implements Runnable,KeyListener{
    private DisplayMode dm;
    private BaseSprite bg;
    private BaseSprite earth;
    private Ship ship;
    private Asteroid asteroid;
    private BaseSprite explosion;
    private BaseLayerManager lm;
    private int scores; //сколько уничтожено астероидов
    private boolean gameOver; //окончена ли игра
    
    
    public GamePanel(DisplayMode dm) {
        try {
        this.dm=dm;
        this.setSize(dm.getWidth(),dm.getHeight());  
        this.addKeyListener(this);
        bg=new BaseSprite(ImageIO.read(getClass().getResourceAsStream("/bg.png")));
        bg.setPosition(-1000, -1000);
        earth=new BaseSprite(ImageIO.read(getClass().getResourceAsStream("/earth.png")));
        earth.setPosition(dm.getWidth()/2, dm.getHeight()/2);
        earth.defineCollisionRect(new Rectangle(20,20,(int)earth.getWidth()-20,(int)earth.getHeight()-20));
        ship=new Ship(ImageIO.read(getClass().getResourceAsStream("/ship.png")),new Point(earth.getX()+(earth.getWidth()/2),earth.getY()+(earth.getHeight()/2)));
        asteroid=new Asteroid(ImageIO.read(getClass().getResourceAsStream("/asteroid.png")),new Point(earth.getX()+(earth.getWidth()/2),earth.getY()+(earth.getHeight()/2)));
        explosion=new BaseSprite(new BufferedImage[]{
            ImageIO.read(getClass().getResourceAsStream("/explose1.png")),
            ImageIO.read(getClass().getResourceAsStream("/explose2.png")),
            ImageIO.read(getClass().getResourceAsStream("/explose3.png")),
            ImageIO.read(getClass().getResourceAsStream("/explose4.png")),
        });
        explosion.setVisible(false);
        lm=new BaseLayerManager();
        lm.append(bg);
        lm.append(earth);
        lm.append(ship);
        lm.append(asteroid);
        lm.append(explosion);
        } catch (IOException e) {}
        scores=0;
        gameOver=false;
    }
    
    public void play(){
        this.setFocusable(true);
        this.requestFocus();
        new Thread(this).start();
    }
    
    public void run(){
        while(true){
        update();
        repaint();
        if (gameOver){
            try{
            Thread.sleep(5000);
            }catch(Exception e){}
            HeadbandFrame.link.showMenu();
            return;
        }
        if (this.scores==5){
            try{
            Thread.sleep(5000);
            }catch(Exception e){}
            HeadbandFrame.link.showMenu();
            return;
        }
        try{
            Thread.sleep(90);
        }catch(Exception e){}
        }
    }
    
     public void paint(Graphics g){
        super.paint(g);
        Graphics2D g2d=(Graphics2D)g; 
        lm.paint(g2d);
        g2d.setColor(Color.WHITE);
        g2d.setFont(new Font("Segoe print", Font.BOLD, 20));
        g2d.drawString(" Уничтожено астероидов="+scores,(dm.getWidth()/2)-100,40);
        if (gameOver){
        String s="Цивилизация уничтожена!";    
        g2d.setFont(new Font("Segoe print", Font.BOLD, 50));
        g2d.drawString("Цивилизация уничтожена!",(dm.getWidth()/2)-(g2d.getFontMetrics().stringWidth(s)/2),dm.getHeight()/2);
        }
        if (scores==5){
        String s="Вы уничтожили астероиды и спасли планету от гибели!";    
        g2d.setFont(new Font("Segoe print", Font.BOLD, 30));
        g2d.drawString(s,(dm.getWidth()/2)-(g2d.getFontMetrics().stringWidth(s)/2),dm.getHeight()/2);
        }
     }
     
     public void update(){
         ship.update();
         asteroid.update();
         bg.rotate(1);
         lm.setView(ship.getX()-(dm.getWidth()/2), ship.getY()-(dm.getHeight()/2));
         //обработка столкновения ракеты с астероидом
         if (ship.collidesRocket(asteroid)){
             Music.playExplose(HeadbandFrame.link);
             explosion.setPosition(asteroid.getX(), asteroid.getY());
             explosion.setFrame(0);
             explosion.setVisible(true);
             asteroid.newPosition();
             ship.exploseRocket();
             scores++;
         }
         //обработка спрайта взрыва
         if (explosion.isVisible()){
             explosion.nextFrame();
             if (explosion.getFrameIndex()==explosion.getFrameSequenceLength()-1){
                 explosion.setVisible(false);
                 explosion.setPosition(-10000, -10000);
             }
         }
         //обработка столкновения астероида с Землей
         if (earth.collidesWith(asteroid) || asteroid.collidesWith(ship)) gameOver=true;
     }
     
    
     
     
     
        public void keyPressed(KeyEvent ke) {
            switch(ke.getKeyCode()){
                case KeyEvent.VK_LEFT:ship.rotate(5);
                                      break;
                case KeyEvent.VK_RIGHT:ship.rotate(-5);
                                      break;  
                case KeyEvent.VK_SPACE:ship.startRocket();
                                      break;
            }
        }
        
        public void keyTyped(KeyEvent ke) {}
       
        public void keyReleased(KeyEvent ke) {}



}
