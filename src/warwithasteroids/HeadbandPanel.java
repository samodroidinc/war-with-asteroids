/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package warwithasteroids;

import java.awt.Color;
import java.awt.DisplayMode;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 *
 * @author Сергей
 */
public class HeadbandPanel extends JPanel   implements Runnable{
    private DisplayMode dm;
    private int numberHeadband;
    private BufferedImage h1,h2,h3,h4; //картинки с заставками
    private String[] titles={
    "Компания Самодроид представляет",
    "2014 год. На Землю движутся астероиды..." ,
    "...диаметр каждого не меньше одного километра...",
    "...ученые создают проект корабля с боезарядами.."
    }; //текст заставок
    
    public HeadbandPanel(DisplayMode dm) {
        this.dm=dm;
        numberHeadband=1;
        try {
            h1=ImageIO.read(getClass().getResourceAsStream("/headband1.png"));
            h2=ImageIO.read(getClass().getResourceAsStream("/headband2.png"));
            h3=ImageIO.read(getClass().getResourceAsStream("/headband3.png"));
            h4=ImageIO.read(getClass().getResourceAsStream("/headband4.png"));
        } catch (IOException ex) {}
        this.setSize(dm.getWidth(),dm.getHeight());
        Music.playStart1(HeadbandFrame.link);
    }
    
    public void play(){
        new Thread(this).start();
    }
    
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        switch(numberHeadband){
            case 1:g.setColor(Color.black);
                  g.fillRect(0, 0, dm.getWidth(), dm.getHeight());
                  g.drawImage(h1, (dm.getWidth()/2)-(h1.getWidth()/2), dm.getHeight()/5,null);
                  g.setColor(Color.WHITE);
                  g.setFont(new Font("Arial",Font.PLAIN, 25));
                  g.drawString(titles[0], (dm.getWidth()/2)-(g.getFontMetrics().stringWidth(titles[0])/2),dm.getHeight()/2);
                  break;  
            case 2:g.drawImage(h2,0,0,null);
                  g.setColor(Color.WHITE);
                  g.setFont(new Font("Arial",Font.PLAIN, 25));
                  g.drawString(titles[1], (dm.getWidth()/2)-(g.getFontMetrics().stringWidth(titles[1])/2),dm.getHeight()/2);
                  break;
            case 3:g.drawImage(h3,0,0,null);
                  g.setColor(Color.WHITE);
                  g.setFont(new Font("Arial",Font.PLAIN, 25));
                  g.drawString(titles[2], (dm.getWidth()/2)-(g.getFontMetrics().stringWidth(titles[2])/2),dm.getHeight()/2);
                  break;
            case 4:g.setColor(Color.WHITE);
                  g.fillRect(0, 0, dm.getWidth(), dm.getHeight());
                  g.drawImage(h4,(dm.getWidth()/2)-(h4.getWidth()/2),0,null);
                  g.setColor(Color.BLACK);
                  g.setFont(new Font("Arial",Font.PLAIN, 25));
                  g.drawString(titles[3], (dm.getWidth()/2)-(g.getFontMetrics().stringWidth(titles[3])/2),h4.getHeight()+50);
                  break;      
        }
    }
    
   public void run(){
       try{
           repaint();
           Thread.sleep(6000);
           numberHeadband++;
           repaint();
           Thread.sleep(6000);
           numberHeadband++;
           repaint();
           Thread.sleep(6000);
           numberHeadband++;
           repaint();
           Thread.sleep(9000);
       }catch(Exception e){}
       HeadbandFrame.link.showMenu();
       Music.playStart2(HeadbandFrame.link);
   }
    
    
}
