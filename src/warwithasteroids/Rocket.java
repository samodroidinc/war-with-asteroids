package warwithasteroids;

import java.awt.image.BufferedImage;


public class Rocket extends BaseSprite{
    private int speedX,speedY; //скорости по осям
    private boolean starting; //запущена ли ракета
    private int maxDistance; //максимальная дистанция
    private int distance;
    
    public Rocket(BufferedImage image) {
        super(image);
        speedX=speedY=distance=0;
        maxDistance=700;
        starting=false;
    }
    
    //запуск ракеты
    public void start(double degress){
        speedX=(int)(Math.cos(Math.toRadians(degress-95))*10);
        speedY=(int)(Math.sin(Math.toRadians(degress-95))*10);
        starting=true;
        distance=0;
    }
    
    public void explose(){
        starting=false;
    }
    
    public void update(){
        if (starting){
            move(speedX,speedY);
            distance+=Math.abs(speedX)+Math.abs(speedY);
            if (distance>maxDistance) starting=false;
        }
        
    }
    
    public boolean isStarting(){
        return starting;
    }
    
    
}
