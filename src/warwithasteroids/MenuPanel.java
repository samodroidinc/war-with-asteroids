package warwithasteroids;

import java.awt.DisplayMode;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Сергей
 */
public class MenuPanel extends JPanel implements ActionListener{
    private DisplayMode dm;
    private BufferedImage bg;
    private JButton button1,button2;
    
    public MenuPanel(DisplayMode dm) {
        this.dm=dm;
        this.setLayout(null);
        try {
        bg=ImageIO.read(getClass().getResourceAsStream("/headband3.png"));
        } catch (IOException e) {}
        button1=new JButton("Игра");
        add(button1);
        button1.setSize(200, 50);
        button1.setLocation((dm.getWidth()/2)-(button1.getWidth()/2), dm.getHeight()/2);
        button1.addActionListener(this);
        button2=new JButton("Помощь");
        add(button2);
        button2.setSize(200, 50);
        button2.setLocation((dm.getWidth()/2)-(button2.getWidth()/2), (dm.getHeight()/2)+90);
        button2.addActionListener(this);
        this.setSize(dm.getWidth(),dm.getHeight());
    }
    
    public void play(){
        repaint();
    }
    
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        g.drawImage(bg, 0, 0,null);
    }
    
    public void actionPerformed(ActionEvent e) {
      if (e.getActionCommand().equals("Помощь"))JOptionPane.showMessageDialog(null,"Управление:стрелки-поворот,пробел-пуск ракеты.\nКомпания 'Самодроид',2014 год,\nидея-Владислав Волков,\nпрограммирование-Сергей Самоделкин,\nпомощь- Сергей Макаров и Федор Долетов.");
      if (e.getActionCommand().equals("Игра")) HeadbandFrame.link.showGame();
    }
    
}
