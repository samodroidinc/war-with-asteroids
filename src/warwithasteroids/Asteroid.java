/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package warwithasteroids;

import java.awt.DisplayMode;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.Random;

/**
 *
 * @author Сергей
 */
public class Asteroid extends BaseSprite{
    private Point center; //точка центра планеты
    private Random rnd;
    private int speedX,speedY;
    public int s; //пройденный путь
    
    public Asteroid(BufferedImage image,Point center) {
        super(image);
        this.center=center;
        rnd=new Random();
        speedX=speedY=0;
        newPosition();
    }
    
    public void newPosition(){
        int degress=rnd.nextInt(360);
        speedX=(int)(Math.cos(Math.toRadians(degress))*10)/2;
        speedY=(int)(Math.sin(Math.toRadians(degress))*10)/2;
        setPosition((int)(Math.cos(Math.toRadians(degress))*900+center.getX()),(int)(Math.sin(Math.toRadians(degress))*900+center.getY()));
        s=0;
    }
    
    public void update(){
    move(-speedX,-speedY);    
    rotate(2);
    s++;
    if (s>1100) newPosition();
    }
    
}
