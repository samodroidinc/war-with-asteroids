/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package warwithasteroids;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class HeadbandFrame extends JFrame{
private HeadbandPanel headbandPanel; //заставка
private MenuPanel menu; //меню
private GamePanel game; //игровая панель
private DisplayMode dm;
public static HeadbandFrame link;

     HeadbandFrame(String title){
      super(title);
      link=this;
      setVisible(true);
      this.setDefaultCloseOperation(EXIT_ON_CLOSE);
      dm=getGraphicsConfiguration().getDevice().getDisplayMode();
      setSize(dm.getWidth(),dm.getHeight());
      this.setResizable(false);
      try{
      this.setUndecorated(true);
      }catch(Exception e){}
      headbandPanel=new HeadbandPanel(dm);
      this.setContentPane(headbandPanel);
      headbandPanel.play();
    }
     
     public void showMenu(){
     menu=new MenuPanel(dm);
     this.setContentPane(menu);
     menu.play();
     }
     
     public void showGame(){
         game=new GamePanel(dm);
         this.setContentPane(game);
         game.play();
     }
    
    
    public static void main(String[] args) {
    HeadbandFrame headband=new HeadbandFrame("Война с астероидами");
    }
    
}
