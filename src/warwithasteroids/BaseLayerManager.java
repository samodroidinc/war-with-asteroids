/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warwithasteroids;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Vector;

public class BaseLayerManager extends BaseLayer {

    private Vector<BaseLayer> layers;
    private int xView, yView;

    public BaseLayerManager() {
        super();
        layers = new Vector<BaseLayer>();
        xView = yView = 0;
    }

    public void append(BaseLayer layer) {
        layers.add(layer);
    }

    public void insert(BaseLayer layer, int index) {
        layers.insertElementAt(layer, index);
    }

    public void remove(BaseLayer layer) {
        layers.remove(layer);
    }

    public void setView(int xView, int yView) {
        this.xView = xView;
        this.yView = yView;
    }

    public void paint(Graphics2D g2d) {
        g2d.translate(-xView, -yView);
        for (int i = 0; i < layers.size(); i++) {
            layers.elementAt(i).paint(g2d);
        }
        g2d.translate(xView,yView);
    }

    public int getSize() {
        return layers.size();
    }

    public BaseLayer getLayerAt(int index) {
        return layers.elementAt(index);
    }

    public void move(int dx, int dy) {
        for (int i = 0; i < layers.size(); i++) {
            layers.elementAt(i).move(dx, dy);
        }
    }

}
